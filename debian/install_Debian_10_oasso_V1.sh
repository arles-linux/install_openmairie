#!/bin/bash
#
# install_Debian_10_oasso_V1.sh
# ============================================
# Installation complete OpenMairie Arles-linux
# - Jean-pierre Antinoux
# - Francois Raynaud
# - Olivier Carpels
# - Ludovic Bourely
# 11 avril 2020 V1
# ============================================
# Version Debian 10 (Buster)
# par defaut php 7.3
# ============================================
# OpenAssociation fichier sur la forge de Adullact
# Exemple openassociation_1.0.1.zip du projet Openassociation
# https://adullact.net/projects/openassociation/

telechargement=https://adullact.net/frs/download.php/file/8344/openassociation_1.0.1.zip
archive=openassociation_1.0.1.zip
base=openstock
url=openassociation_1.0.1/data/pgsql

# version postgresql et postgis suivant version dans la distribution
pgsql="11"
postgis="2.5"
# nom du paquet postgresXX-postgisXX suivant version
tmp1="postgresql-$pgsql-postgis-$postgis"
# emplacement de pg_hba.conf(tmp2) avec nom de la copie(tmp3)
tmp2="/etc/postgresql/$pgsql/main/pg_hba.conf"
tmp3="/etc/postgresql/$pgsql/main/pg_hba.confold"
# =============================================

# Teste si le fichier pg_hba.conf est présent dans le répertoire"
if [ -f "pg_hba.conf" ] ; then
	apt update -y
	# Si Debian est installé en version minimale il faut ajouter :
	apt install -y wget unzip
	echo "=================================================="
	echo "La mise à jour de la liste des paquets à été faite"
	echo "=================================================="
	sleep 5

	# =====================
	# Installation apache2
	# =====================
	echo "*** install apache2"
	apt install -y apache2
	echo "======================"
	echo "apache2 a été installé"
	echo "======================"
	sleep 5

	# ====================
	# Install php
	# ====================
	echo "*** installe php "
	apt install -y php 
	echo "*** install php-pgsql"
	apt install -y php-pgsql
	echo "install php-mbstring"
	apt install -y php-mbstring
	echo "install php-xml" 
	apt install -y php-xml
	echo "*** redemarre apache pour prise en compte de librairie "
	service apache2 restart
	echo "========================================================"
	echo "les 5 paquets php ont été installés et apache2 redémmaré"
	echo "========================================================"
	sleep 5

	# ========================
	# Installation de postgres
	# ========================
	echo "*** install postgresql"
	apt install -y postgresql
	echo "========================"
	echo "postgress a été installé"
	echo "========================"
	sleep 5

	# =======================
	# Installation de postgis
	# =======================
	echo "*** install postgresql-contrib"
	apt install -y postgresql-contrib
	echo "*** install postgis"
	apt install -y postgis
	echo "*** install du paquet $tmp1"
	apt install -y $tmp1
	echo "*** install du paquet $tmp1-scripts"
	apt install -y $tmp1-scripts
	echo "==================================="
	echo "4 paquets postgis ont été installés"
	echo "==================================="
	sleep 5

	# =============================
	# Gestion des acces postgresql
	# =============================
        echo "*** gestion des acces a postgresql" 
        echo "*** copie de sauvegarde $tmp2 "
        cp $tmp2 $tmp3
        echo "*** copie de pg_hba.conf dans $tmp2" 
        cp pg_hba.conf $tmp2
        echo "*** redemarre postgres" 
        service postgresql restart
        echo "==========================================================="
        echo "Le fichier pg_hba.conf a été installé et postgresql relancé"
        echo "==========================================================="
        sleep 5

	# =======================
	# installation openMairie
	# =======================
	echo "*** droit d'ecriture et lecture sur var/www/html (a affiner)"
	chmod 777 -R /var/www/html/
	cd /var/www/html
	echo "*** telechargement" $telechargement
	wget $telechargement
	echo "*** decompression" $archive
	unzip $archive
	echo "*** creation de la base" $base
	createdb -U postgres $base
	echo "*** creation du shema dans le base" $base
	echo "*** depuis $url"
	cd $url
	psql -U postgres $base < install.sql
	echo "==============================================="
	echo "*** Si COMMIT en fin d'installation : Schema OK"
	echo "==============================================="
else
	echo "======================================================="
	echo "La procédure ne peut s'executer, il manque le fichier :"
	echo "*****      pg_hba.conf     *****"
	echo "Contatez l'administrateur à l'adresse suivante :"
	echo "https://openmairie.org"
	echo "======================================================="
fi

# ---/---
