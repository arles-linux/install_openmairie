#!/bin/bash
#
# 02_permission_linux.sh
# ============================================
# Permission Linux -  OpenMairie Arles-linux
# 	- Jean-pierre Antinoux
#	- François Raynaud
# 11 mai 2020
# ============================================
# 	Version Debian 10 (Buster)
# 	Par defaut php 7.3
# ============================================
# création du dossier var à la racine
urlbase="/var/www/html/openassociation_1.0.1/"
# =============================================

echo "*** Création du répertoire var ***"
mkdir $urlbase/var
echo " *** Creation fichier index.php pour empecher acces a var ***"
cp $urlbase/sql/index.php $urlbase/var/index.php
echo " *** Limitations des droits sur les répertoires ***" $urlbase
chown -R root:www-data $urlbase
# find $urlbase -type f -print0 | xargs -0 chmod -t,a-rwxs,u+rw,g+r
find $urlbase -type f -print0 | xargs -0 chmod 640 
# find $urlbase -type d -print0 | xargs -0 chmod -t,a-rwxs,u+rwx,g+rx
find $urlbase -type d -print0 | xargs -0 chmod 750
echo " *** Pouvoir écrire dans var pour les logs et les fichiers ***"
chmod -R g+w $urlbase/var

# ---/---
