#!/bin/bash

chemin="/root/pg1"

# sed -i -e "s/machaines1/machaine2/g" fichier


sed -i -e "s/local   all             postgres                                md5/local   all             postgres                                trust/g" $chemin

sed -i -e "s/local   all             all                                     md5/local   all             all                                     trust/g" $chemin

sed -i -e "s/host    all             all             127.0.0.1/32            md5/host    all             all             127.0.0.1/32            trust/g" $chemin

sed -i -e "s/host    all             all             ::1/128                 md5/host    all             all             ::1/128                 trust/g" $chema

sed -i -e "s/local   replication     all                                     md5/local   replication     all                                     peer/g" $chemin
