#!/bin/bash

# verrou_droits.sh
# JPA 04-2020
# Ce script permet de sécuriser individuelement les fichiers
# puis les dossiers du répertoire www
# Source : https://www.debian-fr.org/t/apache-reglage-des-droits/52588

chown -R root:www-data /var/www
find /var/www/ -type f -print0 | xargs -0 chmod -t,a-rwxs,u+rw,g+r
find /var/www -type d -print0 | xargs -0 chmod -t,a-rwxs,u+rwx,g+rx

# Ligne rajoutée pour permettre l'écriture des archives.
chmod -R g+w /var/www/html/openassociation_1.0.1/var

# Il reste à trouver pourquoi on peut toujours entrer dans le dossier "var"
# malgré la présense du "index.php" dans le répertoire "var"


