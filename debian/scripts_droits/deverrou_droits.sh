#!/bin/bash

# deverrou_droits.sh
# JPA 04-2020
# Ce script permet de libérer individuelement les fichiers
# puis les dossiers du répertoire www
# Source : https://www.debian-fr.org/t/apache-reglage-des-droits/52588

chown -R www-data:www-data /var/www
find /var/www -type f -print0 | xargs -0 chmod -t,a-rwxs,u+rw
find /var/www -type d -print0 | xargs -0 chmod -t,a-rwxs,u+rwx
