#!/bin/bash
#
# install_mint_18_exec.sh
# ============================================
# Installation complete OpenMairie Arles-linux
# - Jean-pierre Antinoux
# - Francois Raynaud
# - Olivier Carpels
# - Ludovic Bourely
# 10 avril 2020
# ================================
# Version linux mint 18.2 19.3
# par defaut php 7.2
# ================================
# OpenAssociation fichier sur la forge de Adullact
# Exemple openassociation_1.0.1.zip du projet Openassociation
# https://adullact.net/projects/openassociation/
telechargement=https://adullact.net/frs/download.php/file/8344/openassociation_1.0.1.zip
archive=openassociation_1.0.1.zip
base=openstock
url=openassociation_1.0.1/data/pgsql
# version postgresql et postgis suivant version dans la distribution
pgsql="10"
postgis="2.4"
# nom du paquet postgresXX-postgisXX suivant version
tmp1="postgresql-$pgsql-postgis-$postgis"
# emplacement de pg_hba.conf(tmp2) avec nom de la copie(tmp3)
tmp2="/etc/postgresql/$pgsql/main/pg_hba.conf"
tmp3="/etc/postgresql/$pgsql/main/pg_hba.confold"
sudo apt update -y
# =====================
# Installation apache2
# =====================
echo "*** install apache2"
sudo apt install -y apache2 
# ============
# Install php
# ============
echo "*** installe php "
sudo apt install -y php 
echo "*** install php-pgsql"
sudo apt install -y php-pgsql
echo "install php-mbstring"
sudo apt install -y php-mbstring
echo "install php-xml" 
sudo apt install -y php-xml
echo "*** redemarre apache pour prise en compte de librairie "
sudo service apache2 restart
# ========================
# Installation de postgres
# ========================
echo "*** install postgresql"
sudo apt install -y postgresql
# =======================
# installation de postgis
# =======================
echo "*** install postgresql-contrib"
sudo apt install -y postgresql-contrib
echo "*** install postgis"
sudo apt install -y postgis
echo "*** install du paquet $tmp1"
sudo apt install -y $tmp1
echo "*** install du paquet $tmp1-scripts"
sudo apt install -y $tmp1-scripts
# =============================
# Gestion des acces postgresql
# =============================
echo "*** gestion des acces a postgresql" 
echo "*** copie de sauvegarde $tmp2 "
sudo cp $tmp2 $tmp3
echo "*** copie de pg_hba.conf dans $tmp2" 
sudo cp pg_hba.conf $tmp2
echo "*** redemarre postgres" 
sudo service postgresql restart
# Visuel des modifications dans le fichier pg_hba.conf
#---------------------------------------------------------------------
# Database administrative login by Unix domain socket
#local   all             postgres                                trust
# TYPE  DATABASE        USER            ADDRESS                 METHOD
# "local" is for Unix domain socket connections only
#local   all             all                                     trust
# IPv4 local connections:
#host    all             all             127.0.0.1/32            trust
#-------------------------------------------------------------------
# =======================
# installation openMairie
# =======================
echo "*** droit d'ecriture et lecture sur var/www/html (a affiner)"
sudo chmod 777 -R /var/www/html/
cd /var/www/html
echo "*** telechargement : $telechargement"
wget $telechargement
echo "*** decompression "$archive
unzip $archive
echo "*** creation de la base $base"
createdb -U postgres $base
echo "*** creation du shema dans le base "$base
echo "*** depuis $url"
cd $url
sudo psql -U postgres $base < install.sql
echo "*** Si commit Installation shema ok "

